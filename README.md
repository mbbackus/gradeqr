# Copy & Paste the GradeQR Code
1. Click on "Tools" >> "Script editor..."
2. Name the script something your students will recognize (for example: Backus - Gradebook)
3. Copy and paste Code.gs code from Bitbucket to Script editor
4. Create gradebook.html in Script Editor, then copy and paste code from Bitbucket
5. Create qrCodeGenerator.html in Script Editor, then copy and paste code from Bitbucket
6. Create getQRCode.html in Script Editor, then copy and paste code from Bitbucket

# Setup your gradebook
1. Change name of the tab you plan to use for your gradebook to "Gradebook"
2. Run the setId function in the Script Editor
3. See the [GradeQR Demo](https://goo.gl/3HduYD) spreadsheet for an example on how to setup your gradebook
4. Delete all blank rows and columns.
5. Add notes to the cells of the assignments in your spreadsheet to make requirements show up below the QR code.
6. Use a pipe symbol between the items you want to keep hidden and the items you want to show the user.


# Publish the Web App
1. Save all files.
2. Click "Publish" >> "Deploy as web app..."
3. Select "New" from the "Project version" dropbox.
4. In the "Execute the app as" dropbox, choose "User accessing the web app"
5. In the "Who has access to the app:" dropbox, choose "Anyone"
6. Click "Deploy"
7. Click "OK"
8. The URL in the textbox is your web app's URL. It executes the code you had when you last published your app. It will not work until append the query strings mentioned below.
9. If you get an error that says: "Sorry, unable to opent the file at this time." you may need to delete the following characters from your URL: /u/0

# Share the Spreadsheet
1. Change Share settings to "On - Public on the web"

# Install and Setup the Android App
1. Download and install the GradeQR.apk file from the source files section of this repository.
2. When prompted, copy and paste your web app's URL into the textbox that appears the first time the app runs.

# Use GradeQR
1. Add a few students and assigments as shown in the Demo web app
2. To get a QR code, copy and paste the URL for your web app into a new tab and add the following to the end: ?task=getQRCode
3. Scan the QR code with your Android device.
4. To view your grades, copy and paste the URL for your web app into a new tab and add the following to the end: ?task=getGrades

# Use the Demo
1. Send me an email and I will add you to the demo
2. To access your information within the gradebook, go to: [https://script.google.com/a/apps.matsuk12.us/macros/s/AKfycbzpVFHdxA7YB4MxMtdFnKMBRmYTphlryg81x0JDRyaaTUHg9sLH/exec?task=getGrades](https://script.google.com/a/apps.matsuk12.us/macros/s/AKfycbzpVFHdxA7YB4MxMtdFnKMBRmYTphlryg81x0JDRyaaTUHg9sLH/exec?task=getGrades)
3. To access an assignment QR code, go to: [https://script.google.com/a/apps.matsuk12.us/macros/s/AKfycbzpVFHdxA7YB4MxMtdFnKMBRmYTphlryg81x0JDRyaaTUHg9sLH/exec?task=getQRCode](https://script.google.com/a/apps.matsuk12.us/macros/s/AKfycbzpVFHdxA7YB4MxMtdFnKMBRmYTphlryg81x0JDRyaaTUHg9sLH/exec?task=getQRCode)