//Used to include files in other files
function include(filename) {
  return HtmlService.createHtmlOutputFromFile(filename).getContent();
}

function convertArray1D(sourceArray) {
  var newArray = [];
  var rows = sourceArray.length;
  var columns = sourceArray[0].length;
  if(rows > columns)
    for(var i = 0, len = sourceArray.length; i < len; i++)
      newArray.push(sourceArray[i][0]);
  else
    for(var i = 0, len = sourceArray[0].length; i < len; i++)
      newArray.push(sourceArray[0][i]);
  return newArray;
}

function doGet(e) {
  if(e.parameter.task === "getGrades")
  {
    return HtmlService.createTemplateFromFile('gradebook').evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME);
  }
  else if(e.parameter.task === "getQRCode")
  {
    return HtmlService.createTemplateFromFile('getQRCode').evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME);
  }
  else if(e.parameter.task === "addGrade")
  {
    var gradebook = SpreadsheetApp.openById(PropertiesService.getDocumentProperties().getProperty('spreadsheetId')).getSheetByName('Gradebook');
    var userEmails = gradebook.getRange('A:A').getValues();
    var lastColumn = gradebook.getLastColumn();
    var assignments = gradebook.getRange('1:1').getValues();
    
    var userRow = 0;
    
    //userIDs[userRow] is used to test whether the end of the range has been reached
    while (userEmails[userRow][0] !== e.parameter.studentEmail)
      userRow++;

    var assignmentColumn = 0;
    /*replace(/ /g,'') causes all spaces to be removed from assignment names so they can easily
    be compared to the assignment name sent in the get request (which does not handle spaces well)*/
    while (assignments[0][assignmentColumn].replace(/ /g,'') !== e.parameter.assignment + '*')
      assignmentColumn++;
    
    var currentScore = gradebook.getRange(userRow+1, assignmentColumn+1).getValue();
    if(isNaN(currentScore)) {
      return HtmlService.createHtmlOutput('Grade NOT entered.');
    } else {
      gradebook.getRange(userRow+1, assignmentColumn+1).setValue(e.parameter.score);
      return HtmlService.createHtmlOutput('Grade entered.');
    } 
  }
}

function setId() {
  PropertiesService.getDocumentProperties().setProperty('spreadsheetId', SpreadsheetApp.getActiveSpreadsheet().getId());
  Logger.log(PropertiesService.getDocumentProperties().getProperty('spreadsheetId'));
}

function getGradesForUser() {
  var email = Session.getActiveUser().getEmail();
  var gradebook = SpreadsheetApp.openById(PropertiesService.getDocumentProperties().getProperty('spreadsheetId')).getSheetByName('Gradebook');
  var lastColumn = gradebook.getLastColumn();
  var userEmails = convertArray1D(gradebook.getRange('A:A').getValues());
  
  row = userEmails.indexOf(email) + 1;
  
  if(row === 0)
    return 'Student ID not found. Did you log in with your school account? If not, log out of any Google account and try logging in again.';
  
  var html = '<table>';

  var assignments = gradebook.getRange('1:1').getValues();
  var scores = gradebook.getRange(row,1,1,lastColumn).getValues();
  var colors = gradebook.getRange(row,1,1,lastColumn).getBackgrounds();
  for(var i = convertArray1D(assignments).indexOf('|')+1, len = lastColumn; i < len; i++)
  {
    if(scores[0][i] !== '')
      html += '<tr><td>' + assignments[0][i] + '</td><td style="background-color:' + colors[0][i] + ';">' + scores[0][i] + '</td></tr>';
  }
  
  html += '</table>';
  
  return html;
}

function getQRAssignments() {
  var gradebook = SpreadsheetApp.openById(PropertiesService.getDocumentProperties().getProperty('spreadsheetId')).getSheetByName('Gradebook');
  var lastColumn = gradebook.getLastColumn();
  var assignmentNames = gradebook.getRange('1:1').getValues();
  var assignmentNotes = gradebook.getRange('1:1').getNotes();
  var assignmentPointValues = gradebook.getRange('2:2').getValues();
  var html = 'Choose Assignment: ' + 
    '<select id="assignment" onchange="google.script.run.withSuccessHandler(onGetIDSuccess).getQRCodeData(); canvasStatus();">' +
    '<option></option>';
  for(var i = convertArray1D(assignmentNames).indexOf('|') - 1, len = lastColumn; i < len; i++) {
    var assignment = assignmentNames[0][i];
    if(assignment.substr(assignment.length - 1) === '*')
      html += '<option data-checklist="' + assignmentNotes[0][i] + '" value="' + assignmentNames[0][i].substr(0,assignment.length-1).replace(/ /g,'') + ',' + assignmentPointValues[0][i] + '">' + assignmentNames[0][i].substr(0,assignment.length-1) + '</option>';
  }
  html += '</select>';
  return html;
}

function getQRCodeData() {
  var email = Session.getActiveUser().getEmail();
  var gradebook = SpreadsheetApp.openById(PropertiesService.getDocumentProperties().getProperty('spreadsheetId')).getSheetByName('Gradebook');
  var lastRow = gradebook.getLastRow();  
  var assignmentRow = convertArray1D(gradebook.getRange('1:1').getValues());
  var userEmails = convertArray1D(gradebook.getRange('A:A').getValues());
  var lastNames = gradebook.getRange(1,assignmentRow.indexOf('Last Name')+1,lastRow,1).getValues();
  var firstNames = gradebook.getRange(1,assignmentRow.indexOf('First Name')+1,lastRow,1).getValues();

  row = userEmails.indexOf(email);
  
  if(row === -1)
    return {email:'User not found'}
  else
    return {email:email,name:firstNames[row][0] + " " + lastNames[row][0].substr(0,1)}
}